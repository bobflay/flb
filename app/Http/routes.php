<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {

$useragent=$_SERVER['HTTP_USER_AGENT'];

    return view('welcome');
});

Route::get('/test',function(){
	return view('test');
});


Route::get('/arabic', function(){
	
	$useragent=$_SERVER['HTTP_USER_AGENT'];

	return view('arabic.welcome');
});


Route::get('/committees',function(){

	$committees = [
		[
			'name'=>'ELIE FARHAT',
			'image' => '/committees/elie_farhat.jpg'
		],
		[
			'name'=>'FADI TABET',
			'image' => '/committees/fadi_tabet.jpg'
		],
		[
			'name'=>'FARES MDAWAR',
			'image' => '/committees/fares_mdawar.jpg'
		],
		[
			'name'=>'FOUAD SALIBA',
			'image' => '/committees/fouad_saliba.jpg'
		],
		[
			'name'=>'GEORGES SABOUNJIAN',
			'image' => '/committees/georges_sabounjian.jpg'
		],
		[
			'name'=>'GHASSAN FARES',
			'image' => '/committees/ghassan_fares.jpg'
		],
		[
			'name'=>'HADI GHAMRAWI',
			'image' => '/committees/hadi_ghamrawi.jpg'
		],
		[
			'name'=>'MAROUN GEBRAYEL',
			'image' => '/committees/maroun_gebrayel.jpg'
		],
		[
			'name'=>'NADER BASMA',
			'image' => '/committees/nader_basma.jpg'
		],
		[
			'name'=>'NAZIH BAWJI',
			'image' => '/committees/nazih_bawji.jpg'
		],
		[
			'name'=>'NIZAR RAWASS',
			'image' => '/committees/nizar_rawass.jpg'
		],
		[
			'name'=>'RAMI FAWAZ',
			'image' => '/committees/rami_fawaz.jpg'
		],
		[
			'name'=>'ROGER ACHKOUY',
			'image' => '/committees/roger_achkouy.jpg'
		],
		[
			'name'=>'VICKEN TCHERTCHIAN',
			'image' => '/committees/vicken_tchertchian.jpg'
		],
	];

	return Response::json($committees);
});

Route::get('/mobile', function(){
	return view('mobile');
});



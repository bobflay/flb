var directors = new Vue({
	el: '#directors-container',
	data:{

		directors:[
			{
				'name':'WALID NASSAR',
				'image' : '/images/directors/walid_nassar.jpg',
				'position':'FLB President & Member of Competitions Commissions (FIBA)'
			},
			{
				'name':'FARES MDAWAR',
				'image' : '/images/committees/fares_mdawar.jpg',
				'position': 'First Vice President'
			},
			{
				'name':'NIZAR RAWASS',
				'image' : '/images/directors/nizar_rawwas.jpg',
				'position': 'Second Vice President'
			},
			{
				'name':'NADER BASMA',
				'image' : '/images/directors/nader_basma.jpg',
				'position': 'Third Vice President'
			},
			{
				'name':'VICKEN JERJIAN',
				'image' : '/images/committees/vicken_tchertchian.jpg',
				'position': 'Fourth Vice President'
			},
			{
				'name':'GHASSAN FARES',
				'image' : '/images/committees/ghassan_fares.jpg',
				'position': 'Secretary-General'
			},
			{
				'name':'ELIE FARHAT',
				'image' : '/images/committees/elie_farhat.jpg',
				'position': 'Treasurer'
			},
			{
				'name':'NAZIH BAWJI',
				'image' : '/images/committees/nazih_bawji.jpg',
				'position': 'Member'
			},
			{
				'name':'GEORGES SABOUNJIAN',
				'image' : '/images/committees/georges_sabounjian.jpg',
				'position': 'Accountant/Member'
			},
			{
				'name':'RAMI FAWAZ',
				'image' : '/images/committees/rami_fawaz.jpg',
				'position':'member'
			},
			{
				'name':'MAROUN GEBRAYEL',
				'image' : '/images/committees/maroun_gebrayel.png',
				'position':'member'
			},
			{
				'name':'HADI GHAMRAWI',
				'image' : '/images/committees/hadi_ghamrawi.png',
				'position':'member'
			},
			{
				'name':'ROGER ACHKOUTY',
				'image' : '/images/committees/roger_achkouy.png',
				'position':'member'
			},
			{
				'name':'FOUAD SALIBA',
				'image' : '/images/committees/fouad_saliba.jpg',
				'position':'member'
			},
			{
				'name':'FADI TABET',
				'image' : '/images/committees/fadi_tabet.png',
				'position':'member'
			},
			
		],

		pointer:1,

		img_0:null,
		img_1:null,
		img_2:null,
		img_3:null


	},
	ready:function(){
		this.img_0 = this.directors[this.pointer-1];
		this.img_1 = this.directors[this.pointer];
		this.img_2 = this.directors[this.pointer+1];
		this.img_3 = this.directors[this.pointer+2];

	},
	methods:{
		next:function(){
			if(this.pointer<12)
			{
				this.pointer = this.pointer+4;
				this.img_0 = this.directors[this.pointer-1];
				this.img_1 = this.directors[this.pointer];
				this.img_2 = this.directors[this.pointer+1];
				this.img_3 = this.directors[this.pointer+2];
			}else if(this.pointer == 12)
			{
				this.pointer = this.pointer+4
				this.img_0 = this.directors[this.pointer-1];
				this.img_1 = false;
				this.img_2 = false;
				this.img_3 = false;
			}

		},
		back:function(){
			if(this.pointer>4)
			{
				this.pointer = this.pointer-4;
				this.img_0 = this.directors[this.pointer-1];
				this.img_1 = this.directors[this.pointer];
				this.img_2 = this.directors[this.pointer+1];
				this.img_3 = this.directors[this.pointer+2];
			}		
		}
	}
});
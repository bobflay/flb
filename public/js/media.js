var divisions = new Vue({
	el: '#media-container',
	
	data:{

		slider:[
			{
				thumb:'/media/thumbs/thumb-1.jpg',
				img:'/media/img-1.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-2.jpg',
				img:'/media/img-2.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-3.jpg',
				img:'/media/img-3.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-4.jpg',
				img:'/media/img-4.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-5.jpg',
				img:'/media/img-5.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-6.jpg',
				img:'/media/img-6.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-7.jpg',
				img:'/media/img-7.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-8.jpg',
				img:'/media/img-8.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-9.jpg',
				img:'/media/img-9.jpg'
			},
		],

		slider2:[
			{
				thumb:'/media/thumbs/thumb-10.jpg',
				img:'/media/img-10.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-11.jpg',
				img:'/media/img-11.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-12.jpg',
				img:'/media/img-12.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-13.jpg',
				img:'/media/img-13.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-14.jpg',
				img:'/media/img-14.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-15.jpg',
				img:'/media/img-15.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-16.jpg',
				img:'/media/img-16.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-17.jpg',
				img:'/media/img-17.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-18.jpg',
				img:'/media/img-18.jpg'
			}
		],
		slider3:[
			{
				thumb:'/media/thumbs/thumb-19.jpg',
				img:'/media/img-19.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-20.jpg',
				img:'/media/img-20.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-21.jpg',
				img:'/media/img-21.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-22.jpg',
				img:'/media/img-22.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-23.jpg',
				img:'/media/img-23.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-24.jpg',
				img:'/media/img-24.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-25.jpg',
				img:'/media/img-25.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-26.jpg',
				img:'/media/img-26.jpg'
			},
			{
				thumb:'/media/thumbs/thumb-27.jpg',
				img:'/media/img-27.jpg'
			}

		],


		selected:''
	},

	ready:function(){


	},

	methods:{

		showImage:function(uri){

			$('#myModal').modal('toggle');
			this.selected=uri;

		}




	}

})
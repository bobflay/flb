var teams = new Vue({
	el: '#teams-container',
	
	data:{

		male:{
			selected:true,
			waba:[
				{
					name:'Omar EL AYOUBI',
					number:4
				},
				{
					name:'Amir SAOUD',
					number:5
				},
				{
					name:'Ali MAHMOUD',
					number:6,
				},
				{
					name:'Wael ARAKJI',

					number:7
				},
				{
					name:'Elias RUSTOM',
					number:8,
				},
				{
					name:'Nadim SOUAID',
					number:9,
				},
				{
					name:'Nadim HAWE',
					number:10,
				},
				{
					name:'Mohamad Ali HAIDAR',
					number:11
				},
				{
					name:'Charles TABET',
					number:12
				},
				{
					name:'Bassel BAWJI',
					number:13
				},
				{
					name:'Jasmon YOUNGBLOOD',
					number:14
				},
				{
					name:'El Cheikh Fadi EL KHATIB',
					number:15
				}	
			],
			asia:[
				{
					name:'Jean ABD EL NOUR',
					number:4
				},
				{
					name:'Amir SAOUD',
					number:5
				},
				{
					name:'Ali HAIDAR Mohamad',
					number:6
				},
				{
					name:'Wael ARAKJI',
					number:7
				},
				{
					name:'Joseph ABY KHERES',
					number:8
				},
				{
					name:'Ahmad IBRAHIM',
					number:9
				},
				{
					name:'Nadim SOUAID',
					number:10
				},
				{
					name:'Rodrigue AKL',
					number:11
				},
				{
					name:'Charles TABET',
					number:12
				},
				{
					name:'Bassel BAWJI',
					number:13
				},
				{
					name:'Jasmon YOUNGBLOOD',
					number:14
				},
				{
					name:'Omar EL AYOUBI',
					number:15
				},
			]
		},
		youth:{
			selected:false,
			asia:[
				{
					name:'Joe ZIADE',
					number:4
				},
				{
					name:'Mohamad CAPTAN',
					number:5
				},
				{
					name:'Nady HACHEM',
					number:6
				},
				{
					name:'Karim ZEINOUN',
					number:7
				},
				{
					name:'Toufic SAKR',
					number:8
				},
				{
					name:'Georges EL BEYROUTY',
					number:9
				},
				{
					name:'Jad NEMER',
					number:10
				},
				{
					name:'Mohamad EL KHATIB',
					number:11
				},
				{
					name:'Nader BELMONA',
					number:12
				},
				{
					name:'Charbel SAAD',
					number:13
				},
				{
					name:'Mitchel TABET',
					number:14
				},
				{
					name:'Salim ALAAEDDINE',
					number:15
				},
			],
			waba:[
				{
					name:'Salim MOUKADDEM',
					number:4
				},
				{
					name:'Mohamad CAPTAN',
					number:5
				},
				{
					name:'Nady HACHEM',
					number:6,
				},
				{
					name:'Hadi CHARARA',

					number:7
				},
				{
					name:'Mitchel TABET',
					number:8,
				},
				{
					name:'Georges EL BEYROUTY',
					number:9,
				},
				{
					name:'Jad NEMER',
					number:10,
				},
				{
					name:'Sami EL KHATIB',
					number:11
				},
				{
					name:'Nader BELMONA',
					number:12
				},
				{
					name:'Charbel SAAD',
					number:13
				},
				{
					name:'Michael Jad DOUNIA',
					number:14
				},
				{
					name:'Salim ALAAEDDINE',
					number:15
				}	
			],
			arabs:[
				{
					name:'Adel Hanna RIZK',
					number:1
				},
				{
					name:'Ali Mohamad Ziad MANSOUR',
					number:2
				},
				{
					name:'Dani Walid ELKHOURY',
					number:3,
				},
				{
					name:'Elie Joe Salim ELDANI',
					number:4
				},
				{
					name:'Carl Endraos ASSI',
					number:5,
				},
				{
					name:'Mansour Walid ELKHWEIRY',
					number:6,
				},
				{
					name:'Marc Hassib ELKHOURY',
					number:7,
				},
				{
					name:'Naiim Elie RABIEH',
					number:8
				},
				{
					name:'Wiliam Nabil SAWAN',
					number:9
				},
				{
					name:'Ibrahim Amer HADDAD',
					number:10
				},
				{
					name:'Naji AZIZ',
					number:11
				},
				{
					name:'Sami Mohamad ELGHANDOUR',
					number:12
				}	
			]

		},

		group:null,
		table:null,
		d1:false,
		d2:false,
		d3:false,
		d4:false,
		d5:false,

		tab_0_selected:true,
		tab_1_selected:false,
		tab_2_selected:false,
		tab_3_selected:false


	},

	ready:function(){
		this.group = 'male';
		this.table = this.male.asia;
		this.d1 = true;
		this.d2 = true;
		this.d3 = false;

	},

	methods:{

		selectMale:function(){
			this.male.selected = true;
			this.youth.selected = false;
			this.group = 'male';
			this.d3 = false;
			this.selectD1();

		},

		selectYouth:function(){
			this.youth.selected = true;
			this.male.selected = false;
			this.group = 'youth';
			this.d3 = true;
			this.selectD1();
		},

		selectD1:function(){
			this.table = null;

			this.tab_0_selected = true;
			this.tab_1_selected = false;
			this.tab_2_selected = false;
			this.tab_3_selected = false;

			if(this.group == 'male')
			{
				this.table = this.male.asia;
			}else if(this.group == 'youth'){
				this.table = this.youth.asia;
			}
		},
		selectD2:function(){

			this.tab_0_selected = false;
			this.tab_1_selected = true;
			this.tab_2_selected = false;
			this.tab_3_selected = false;


			if(this.group == 'male')
			{
				this.table = this.male.waba;
			}else if(this.group == 'youth'){
				this.table = this.youth.waba;
			}		
		},
		selectD3:function(){
			this.table = null;

			this.tab_0_selected = false;
			this.tab_1_selected = false;
			this.tab_2_selected = true;
			this.tab_3_selected = false;


			if(this.group == 'male')
			{
			}else if(this.group == 'youth'){
				this.table = this.youth.arabs;
			}		
		},




	}

})
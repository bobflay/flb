var directors = new Vue({
	el: '#directors-container',
	data:{

		directors:[
			{
				'name':'وليد نصّار',
				'image' : '/images/directors/walid_nassar.png',
				'position':'الرئيس'
			},
			{
				'name':'فارس مدوّر',
				'image' : '/images/committees/fares_mdawar.jpg',
				'position': 'نائب أول للرئيس'
			},
			{
				'name':'نزار رواس',
				'image' : '/images/committees/nizar_rawass.png',
				'position': 'نائب ثانٍ للرئيس'
			},
			{
				'name':'نادر بسمة',
				'image' : '/images/directors/nader_basma.png',
				'position': 'نائب ثالث للرئيس'
			},
			{
				'name':'ڤيكين جرجيان',
				'image' : '/images/committees/vicken_tchertchian.png',
				'position': 'نائب رابع للرئيس'
			},
			{
				'name':'غسّان فارس',
				'image' : '/images/committees/ghassan_fares.png',
				'position': 'أمين عام]'
			},
			{
				'name':'إيلي فرحات',
				'image' : '/images/committees/elie_farhat.png',
				'position': 'أمين سر'
			},
			{
				'name':'نزيه بوجي',
				'image' : '/images/committees/nazih_bawji.png',
				'position': 'عضو'
			},
			{
				'name':'جورج صابونجيان',
				'image' : '/images/committees/georges_sabounjian.jpg',
				'position': 'محاسب/ عضو'
			},
			{
				'name':'رامي فوّاز',
				'image' : '/images/committees/rami_fawaz.jpg',
				'position':'عضو'
			},
			{
				'name':'مارون جبرايل',
				'image' : '/images/committees/maroun_gebrayel.png',
				'position':'عضو'
			},
			{
				'name':'هادي غمراوي ',
				'image' : '/images/committees/hadi_ghamrawi.png',
				'position':'عضو'
			},
			{
				'name':'روجيه عشقوتي',
				'image' : '/images/committees/roger_achkouy.png',
				'position':'عضو'
			},
			{
				'name':'فؤاد صليبا',
				'image' : '/images/committees/fouad_saliba.jpg',
				'position':'عضو'
			},
			{
				'name':'فادي تابت',
				'image' : '/images/committees/fadi_tabet.png',
				'position':'عضو'
			},
			
		],

		pointer:1,

		img_0:null,
		img_1:null,
		img_2:null,
		img_3:null


	},
	ready:function(){
		this.img_0 = this.directors[this.pointer-1];
		this.img_1 = this.directors[this.pointer];
		this.img_2 = this.directors[this.pointer+1];
		this.img_3 = this.directors[this.pointer+2];

	},
	methods:{
		next:function(){
			if(this.pointer<12)
			{
				this.pointer = this.pointer+4;
				this.img_0 = this.directors[this.pointer-1];
				this.img_1 = this.directors[this.pointer];
				this.img_2 = this.directors[this.pointer+1];
				this.img_3 = this.directors[this.pointer+2];
			}else if(this.pointer == 12)
			{
				this.pointer = this.pointer+4
				this.img_0 = this.directors[this.pointer-1];
				this.img_1 = false;
				this.img_2 = false;
				this.img_3 = false;
			}

		},
		back:function(){
			if(this.pointer>4)
			{
				this.pointer = this.pointer-4;
				this.img_0 = this.directors[this.pointer-1];
				this.img_1 = this.directors[this.pointer];
				this.img_2 = this.directors[this.pointer+1];
				this.img_3 = this.directors[this.pointer+2];
			}		
		}
	}
});
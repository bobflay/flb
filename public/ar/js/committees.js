var committees = new Vue({
	el: '#committees-container',
	data:{

		committees:[
			
			{
				'name':'فادي تابت',
				'image' : '/images/committees/fadi_tabet.png',
				'position':'عضو'
			},
			{
				'name':'مارون جبرايل',
				'image' : '/images/committees/maroun_gebrayel.png',
				'position':'عضو'
			},
			{
				'name':'هادي غمراوي',
				'image' : '/images/committees/hadi_ghamrawi.png',
				'position':'عضو'
			},
			{
				'name':'رامي فواز',
				'image' : '/images/committees/rami_fawaz.jpg',
				'position':'عضو'
			},
			{
				'name':'روجيه عشقوتي',
				'image' : '/images/committees/roger_achkouy.png',
				'position':'عضو'
			},
			{
				'name':'فؤاد صليبا',
				'image' : '/images/committees/fouad_saliba.jpg',
				'position':'عضو'
			}

		],

		pointer:1,

		img_0:null,
		img_1:null,
		img_2:null,
		img_3:null


	},
	ready:function(){
		this.img_0 = this.committees[this.pointer-1];
		this.img_1 = this.committees[this.pointer];
		this.img_2 = this.committees[this.pointer+1];
		this.img_3 = this.committees[this.pointer+2];

	},
	methods:{
		next:function(){
			this.img_0 = this.committees[4];
			this.img_1 = this.committees[5];
			this.img_2 = false;
			this.img_3 = false;
		},
		back:function(){
				this.img_0 = this.committees[0];
				this.img_1 = this.committees[1];
				this.img_2 = this.committees[2];
				this.img_3 = this.committees[3];
					
		}
	}
});
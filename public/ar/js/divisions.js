var divisions = new Vue({
	el: '#divisions-container',
	
	data:{

		male:{
			selected:true,
			divisions:[
				[
					'الرياضي',
					'بيبلوس',
					'الحكمة',
					'الشانفيل',
					'المتحد',
					'تضامن',
					'هومنتمن',
					'هوبس',
					'الويزة',
					'انتراينك',
				],
				[
			        'عمشيت',
		            'المركزية جونية',
		            'العمل بكفيا',
		            'فيطرون',
		            'هوبس صور',
		            'احياء الرياضة',
		            'عينطورة',
		            'تبنين',
		            'الجيش',
			        'بجه',
			        'أبناء نبتون',
			        'الانوار',
			        'مار يوسف بيت الككو',
			        'الفداء صيدا',
			        'الاتحاد ميروبا',
			        'الشبيبة'
				],
				[
					'المعهد الانطوني',
	              	'الكهرباء',
	              	'الشباب صور',
	              	'اشبال الجبل درعون',
	              	'قنوبين',
	              	'الاخوة الحدث',
	              	'الوفاء الكرك',
	              	'اطلس',
	              	'برمانا',
	           		'جبل موسى يحشوش',
	           		'بودا',
	           		'شباب حبوش',
	           		'انيبال زحلة',
	           		'الكرملي',
	           		'غزير',
	           		'المريميين جبيل',
	          		'برج عينطورة',
	           		'شباب درعون',
	           		'الشبيبة مزيارة',
	           		'سان جورج',
	           		'الجمهور',
	           		'الشراع طرابلس',
	           		'انترانيك سن الفيل',
					'شباب البترون'
				]
			]

		},
		female:{
			selected:false,
			divisions:[
				[
					'الرياضي بيروت ',
					'هومنتمن انطلياس',
					'الشباب العربي',
					'انترانيك بيروت',
					'المتحد',
					'هوبس',
					'الشبيبة',
					'التضامن'
				],
				[
					'البرج عينطورة',
					'هومنتمن بيروت',
					'الانوار',
					'انترانيك سن الفيل',
					'هوبس المتن',
					'هومنتمن برج حمود ',
					'الجمهور',
					'بيليفرز'
				]
			]
		},


		group:null,
		table:null,
		d1:false,
		d2:false,
		d3:false,
		d4:false,
		d5:false,

		tab_0_selected:true,
		tab_1_selected:false,
		tab_2_selected:false,
		tab_3_selected:false


	},

	ready:function(){
		this.group = 'male';
		this.table = this.male.divisions[0];
		this.d1 = true;
		this.d2 = true;
		this.d3 = true;

	},

	methods:{

		selectMale:function(){
			this.male.selected = true;
			this.female.selected = false;
			this.group = 'male';
			this.d3 = true;
			this.selectD1();

		},

		selectFemale:function(){
			this.female.selected = true;
			this.male.selected = false;
			this.group = 'female';
			this.d3 = false;
			this.selectD1();
		},

		selectD1:function(){
			this.table = null;

			this.tab_0_selected = true;
			this.tab_1_selected = false;
			this.tab_2_selected = false;
			this.tab_3_selected = false;

			if(this.group == 'male')
			{
				this.table = this.male.divisions[0];
			}else if(this.group == 'female'){
				this.table = this.female.divisions[0];
			}
		},
		selectD2:function(){

			this.tab_0_selected = false;
			this.tab_1_selected = true;
			this.tab_2_selected = false;
			this.tab_3_selected = false;


			if(this.group == 'male')
			{
				this.table = this.male.divisions[1];
			}else if(this.group == 'female'){
				this.table = this.female.divisions[1];
			}		
		},
		selectD3:function(){
			this.table = null;

			this.tab_0_selected = false;
			this.tab_1_selected = false;
			this.tab_2_selected = true;
			this.tab_3_selected = false;


			if(this.group == 'male')
			{
				this.table = this.male.divisions[2];
			}else if(this.group == 'female'){
				this.table = this.female.divisions[2];
			}		
		},




	}

})
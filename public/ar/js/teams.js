var teams = new Vue({
	el: '#teams-container',
	
	data:{

		male:{
			selected:true,
			waba:[
				{
					name:'عمر الايوبي',
					number:4
				},
				{
					name:'أمير سعود',
					number:5
				},
				{
					name:'على محمود',
					number:6,
				},
				{
					name:'واءل عرقجي',

					number:7
				},
				{
					name:'الياس رستم',
					number:8,
				},
				{
					name:'نديم سويد',
					number:9,
				},
				{
					name:'نديم حاوي',
					number:10,
				},
				{
					name:'محمد علي حيدر',
					number:11
				},
				{
					name:'شارل تابت',
					number:12
				},
				{
					name:'باسل بوجي',
					number:13
				},
				{
					name:'جاسمون يونغبلود',
					number:14
				},
				{
					name:'الشيخ فادي الخطيب',
					number:15
				}	
			],
			asia:[
				{
					name:'جان عبد النور',
					number:4
				},
				{
					name:'أمير سعود',
					number:5
				},
				{
					name:'محمد علي حيدر',
					number:6
				},
				{
					name:'وائل عرقجي',
					number:7
				},
				{
					name:'جوزيف أبي خريس',
					number:8
				},
				{
					name:'إبراهيم أحمد',
					number:9
				},
				{
					name:'نديم سويد',
					number:10
				},
				{
					name:'رودريغز عقل',
					number:11
				},
				{
					name:'شارل تابت',
					number:12
				},
				{
					name:'باسل بوجي',
					number:13
				},
				{
					name:'جاسمون يونغبلود',
					number:14
				},
				{
					name:'عمر الايوبي',
					number:15
				},
			]
		},
		youth:{
			selected:false,
			asia:[
				{
					name:'جو زيادة',
					number:4
				},
				{
					name:'كابتن محمد',
					number:5
				},
				{
					name:'نادي هاشم',
					number:6
				},
				{
					name:'كريم زينون',
					number:7
				},
				{
					name:'توفيق صقر',
					number:8
				},
				{
					name:'جورج البيروتي',
					number:9
				},
				{
					name:'جاد نمر',
					number:10
				},
				{
					name:'محمد الخطيب',
					number:11
				},
				{
					name:'نادر لمونا',
					number:12
				},
				{
					name:'شربل سعد',
					number:13
				},
				{
					name:'ميشل تابت',
					number:14
				},
				{
					name:'سليم علاء الدين',
					number:15
				},
			],
			waba:[
				{
					name:'سليم مقدم',
					number:4
				},
				{
					name:'كابتن محمد',
					number:5
				},
				{
					name:'نادي هاشم',
					number:6,
				},
				{
					name:'هادي شرارة',

					number:7
				},
				{
					name:'ميشل تابت',
					number:8,
				},
				{
					name:'جورج البيروتي',
					number:9,
				},
				{
					name:'جاد نمر',
					number:10,
				},
				{
					name:'سامي الخطيب',
					number:11
				},
				{
					name:'نادر بلمونا',
					number:12
				},
				{
					name:'شربل سعد',
					number:13
				},
				{
					name:'ميشل جاد دنيا',
					number:14
				},
				{
					name:'سليم علاء الدين',
					number:15
				}	
			],
			arabs:[
				{
					name:'عادل حنا رزق',
					number:1
				},
				{
					name:'علي محمد زياد منصور',
					number:2
				},
				{
					name:'داني وليد الخوري',
					number:3,
				},
				{
					name:'ايلي جو سليم الضاني',
					number:4
				},
				{
					name:'كارل اندراوس عاصي',
					number:5,
				},
				{
					name:'منصور وليد الخويري',
					number:6,
				},
				{
					name:'مارك حسيب الخوري',
					number:7,
				},
				{
					name:'نعيم ايلي رابيه',
					number:8
				},
				{
					name:'وليم نبيل صوان ',
					number:9
				},
				{
					name:'إبراهيم عامر حداد',
					number:10
				},
				{
					name:'ناجي عزير',
					number:11
				},
				{
					name:'سامي محمد الغندور',
					number:12
				}	
			]

		},

		group:null,
		table:null,
		d1:false,
		d2:false,
		d3:false,
		d4:false,
		d5:false,

		tab_0_selected:true,
		tab_1_selected:false,
		tab_2_selected:false,
		tab_3_selected:false


	},

	ready:function(){
		this.group = 'male';
		this.table = this.male.asia;
		this.d1 = true;
		this.d2 = true;
		this.d3 = false;

	},

	methods:{

		selectMale:function(){
			this.male.selected = true;
			this.youth.selected = false;
			this.group = 'male';
			this.d3 = false;
			this.selectD1();

		},

		selectYouth:function(){
			this.youth.selected = true;
			this.male.selected = false;
			this.group = 'youth';
			this.d3 = true;
			this.selectD1();
		},

		selectD1:function(){
			this.table = null;

			this.tab_0_selected = true;
			this.tab_1_selected = false;
			this.tab_2_selected = false;
			this.tab_3_selected = false;

			if(this.group == 'male')
			{
				this.table = this.male.asia;
			}else if(this.group == 'youth'){
				this.table = this.youth.asia;
			}
		},
		selectD2:function(){

			this.tab_0_selected = false;
			this.tab_1_selected = true;
			this.tab_2_selected = false;
			this.tab_3_selected = false;


			if(this.group == 'male')
			{
				this.table = this.male.waba;
			}else if(this.group == 'youth'){
				this.table = this.youth.waba;
			}		
		},
		selectD3:function(){
			this.table = null;

			this.tab_0_selected = false;
			this.tab_1_selected = false;
			this.tab_2_selected = true;
			this.tab_3_selected = false;


			if(this.group == 'male')
			{
			}else if(this.group == 'youth'){
				this.table = this.youth.arabs;
			}		
		},




	}

})
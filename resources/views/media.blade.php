<div id="media-container">
	<div class="row">
		<div class="calendar-title">
			<div class="col-md-6">
				<p class="black">
					<strong>[media]</strong>
				</p>
				<p id="directors-title2">
					<strong>ROOM</strong>
				</p>
			</div>
		</div>
	</div>

	<div class='row'>
		<div id="gallery-container">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">

				    <div class="item active">

				    	<div class="row">
				    		<div class="col-xs-6" style="padding: 0px;">
				    			<div class="row">
				    				<div class="col-xs-12" style="padding: 0px;">
				    					<img class="img-1" @click="showImage(slider[0].img)" v-bind:src="slider[0].thumb">
				    				</div>
				    			</div>
				    			<div class="row">
				    				<div class="col-xs-6" style="padding:0px;">
				    					<img class="img-2" @click="showImage(slider[1].img)" v-bind:src="slider[1].thumb">
				    				</div>
				    				<div class="col-xs-6" style="padding:0px;">
				    					<img class="img-3" @click="showImage(slider[2].img)" v-bind:src="slider[2].thumb ">
				    				</div>
				    			</div>
				    		</div>
				    		<div class="col-xs-6" style="padding: 0px;">
				    			<img class="img-4" @click="showImage(slider[3].img)" v-bind:src="slider[3].thumb">
				    		</div>
				    	</div>

				    	<div class="row">
				    		<div class="col-xs-8" style="padding: 0px;">
				    			<div class="row">
				    				<div class="col-xs-4" style="padding: 0px">
				    					<img class="img-5" @click="showImage(slider[4].img)" v-bind:src="slider[4].thumb">
				    				</div>
				    				<div class="col-xs-8" style="padding: 0px">
				    					<img class="img-6" @click="showImage(slider[5].img)" v-bind:src="slider[5].thumb">
				    				</div>
				    			</div>
				    			<div class="row">
				    				<div class="col-xs-8" style="padding:0px;">
				    					<img class="img-7" @click="showImage(slider[6].img)" v-bind:src="slider[6].thumb">
				    				</div>
				    				<div class="col-xs-4" style="padding:0px;">
				    					<img class="img-8" @click="showImage(slider[7].img)" v-bind:src="slider[7].thumb">
				    				</div>
				    			</div>
				    		</div>
				    		<div class="col-xs-4">
				    			<img class="img-9" @click="showImage(slider[8].img)" v-bind:src="slider[8].thumb" >
				    		</div>
				    	</div>

				    </div>


				    <div class="item">

				    	<div class="row">
				    		<div class="col-xs-6" style="padding:0px;">
				    			<div class="row">
				    				<div class="col-xs-12" style="padding:0px;">
				    					<img class="img-1" @click="showImage(slider2[0].img)" v-bind:src="slider2[0].thumb">
				    				</div>
				    			</div>
				    			<div class="row">
				    				<div class="col-xs-6" style="padding:0px;">
				    					<img class="img-2" @click="showImage(slider2[1].img)" v-bind:src="slider2[1].thumb">
				    				</div>
				    				<div class="col-xs-6" style="padding:0px;">
				    					<img class="img-3" @click="showImage(slider2[2].img)" v-bind:src="slider2[2].thumb">
				    				</div>
				    			</div>
				    		</div>
				    		<div class="col-xs-6" style="padding:0px;">
				    			<img class="img-4" @click="showImage(slider2[3].img)" v-bind:src="slider2[3].thumb">
				    		</div>
				    	</div>

				    	<div class="row">
				    		<div class="col-xs-8" style="padding:0px">
				    			<div class="row">
				    				<div class="col-xs-4" style="padding:0px">
				    					<img class="img-5" @click="showImage(slider2[4].img)" v-bind:src="slider2[4].thumb">
				    				</div>
				    				<div class="col-xs-8" style="padding:0px;">
				    					<img class="img-6" @click="showImage(slider2[5].img)" v-bind:src="slider2[5].thumb">
				    				</div>
				    			</div>
				    			<div class="row">
				    				<div class="col-xs-8" style="padding: 0px;">
				    					<img class="img-7" @click="showImage(slider2[6].img)" v-bind:src="slider2[6].thumb">
				    				</div>
				    				<div class="col-xs-4" style="padding:0px;">
				    					<img class="img-8" @click="showImage(slider2[7].img)" v-bind:src="slider2[7].thumb">
				    				</div>
				    			</div>
				    		</div>
				    		<div class="col-xs-4">
				    			<img class="img-9" @click="showImage(slider2[8].img)" v-bind:src="slider2[8].thumb" >
				    		</div>
				    	</div>

				    </div>

				    <div class="item">

				    	<div class="row">
				    		<div class="col-xs-6" style="padding:0px;">
				    			<div class="row">
				    				<div class="col-xs-12" style="padding:0px;">
				    					<img class="img-1" @click="showImage(slider3[0].img)" v-bind:src="slider3[0].thumb">
				    				</div>
				    			</div>
				    			<div class="row">
				    				<div class="col-xs-6" style="padding:0px;">
				    					<img class="img-2" @click="showImage(slider3[1].img)" v-bind:src="slider3[1].thumb">
				    				</div>
				    				<div class="col-xs-6" style="padding:0px;">
				    					<img class="img-3" @click="showImage(slider3[2].img)" v-bind:src="slider3[2].thumb">
				    				</div>
				    			</div>
				    		</div>
				    		<div class="col-xs-6" style="padding:0px;">
				    			<img class="img-4" @click="showImage(slider3[3].img)" v-bind:src="slider3[3].thumb">
				    		</div>
				    	</div>

				    	<div class="row">
				    		<div class="col-xs-8" style="padding:0px">
				    			<div class="row">
				    				<div class="col-xs-4" style="padding:0px">
				    					<img class="img-5" @click="showImage(slider3[4].img)" v-bind:src="slider3[4].thumb">
				    				</div>
				    				<div class="col-xs-8" style="padding:0px;">
				    					<img class="img-6" @click="showImage(slider3[5].img)" v-bind:src="slider3[5].thumb">
				    				</div>
				    			</div>
				    			<div class="row">
				    				<div class="col-xs-8" style="padding: 0px;">
				    					<img class="img-7" @click="showImage(slider3[6].img)" v-bind:src="slider3[6].thumb">
				    				</div>
				    				<div class="col-xs-4" style="padding:0px;">
				    					<img class="img-8" @click="showImage(slider3[7].img)" v-bind:src="slider3[7].thumb">
				    				</div>
				    			</div>
				    		</div>
				    		<div class="col-xs-4">
				    			<img class="img-9" @click="showImage(slider3[8].img)" v-bind:src="slider3[8].thumb" >
				    		</div>
				    	</div>

				    </div>

				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" style="width:50px; margin-left:-50px" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" style="width:50px; margin-right:-50px;" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
		</div>
	</div>


	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content large-modal">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      		<img @click="showImage()" class="img-responsive center-block" v-bind:src="selected">
	      </div>
	    </div>
	  </div>
	</div>


</div>
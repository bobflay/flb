<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.5.0/fullcalendar.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<link rel="stylesheet" type="text/css" href="/css/mobile/home.css">

	<title>FLB Website</title>
</head>
<body>

	<div class="container">
		<div class="row">
			<div id="home-container">
				<div class="row">
					<div class="menu">
						<div class="col-md-2">
							Home
						</div>
						<div class="col-md-2">
							The League
						</div>
						<div class="col-md-2">
							Strategy
						</div>
						<div class="col-md-2">
							Clubs
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
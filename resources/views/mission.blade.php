<div id="mission-container">
	<div class="row data-inner">
		<div class="top-bar top-bar-p">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<p id="mission-title1">
					<strong>[The mission of the]</strong>
				</p>
				<p id="mission-title2">
					<strong>FEDERATION</strong>
				</p>
			</div>
		</div>
	</div>

	<div class="images images-p">
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/cup.png">
					<div class="user-data">
						<p class="text-center p-title-m"><strong>ORGANIZING & HOLDING ANNUAL TOURNAMENTS </strong></p>
						<p class="text-center p-body-m">Organizing and holding annual tournaments for the different grades and age groups, for men and women   andorganizing sessions for trainers and preparing courses for referees.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/siren.png">
					<div class="user-data">
						<p class="text-center p-title-m"><strong>SUPERVISING TOURNAMENTS</strong></p>
						<p class="text-center p-body-m">Supervising tournaments that are held on the Lebanese territory and Participating in tournaments and international games outside Lebanon.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/board.png">
					<div class="user-data">
						<p class="text-center p-title-m"><strong>PUBLICISING LAWS & DECISIONS</strong></p>
						<p class="text-center p-body-m">Publicising laws and decisions, and issuing official publications of the Federation.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/basket.png">
					<div class="user-data">
						<p class="text-center p-title-m"><strong>LOCAL TOURNAMENTS</strong></p>
						<p class="text-center p-body-m">Allowing associations to establish special local tournaments and to participate in external tournaments.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<div id="divisions-container">
	<div class="row">
		<div id="divisions-data">
			<div class="divisions-top-bar">
				<div class="col-xs-6 col-sm-6 col-md-6 col-xs-offset-6 col-sm-offset-6 col-md-offset-6 text-right">
					<p id="divisions-title1">
						<strong>[النوادي و]</strong>
					</p>
					<p id="divisions-title2">
						<strong>الانقسامات</strong>
					</p>
				</div>
			</div>	
		</div>
	</div>
	<div class="row">
		<div id="data-table">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="row">
						<div>
							<div class="white col-xs-7 col-sm-7 col-md-7">
								<div id="links-container">
									 <span v-bind:class="{'red':male.selected}" @click="selectMale()">رجال</span> / 
									  <span v-bind:class="{'red':female.selected}" @click="selectFemale()" >نساء</span> /
									  <span class="white">صغار</span>
									 
									

								</div>
							</div>
							<div class="white col-xs-5 col-sm-5 col-md-5" id="clubs">
								<h4 class="text-center">النوادي و الانقسامات</h4>
							</div>

						</div>
					</div>
				</div>

				<div id="arabic-tabs">
					<div v-show="d5" @click="selectD5()" class="col-xs-1 col-sm-1 col-md-1 data-tabs tabs" id="tab-3">
						<h3 class="tab-title text-center">القسم لاخامس</h3>
					</div>
					<div v-show="d4" @click="selectD4()" class="col-xs-1 col-sm-1 col-md-1 data-tabs tabs" id="tab-3">
						<h3 class="tab-title text-center">القسم الرابع</h3>
					</div>
					<div v-show="d3" @click="selectD3()" class="col-xs-1 col-sm-1 col-md-1 data-tabs tabs" id="tab-2" :class="{tab_selected: tab_2_selected}">
						<h3 class="tab-title text-center">القسم الثالث</h3>
					</div>
					<div v-show="d2" @click="selectD2()" class="col-xs-1 col-sm-1 col-md-1 data-tabs tabs" id="tab-1" :class="{tab_selected: tab_1_selected}">
						<h3 class="tab-title text-center">القسم الثاني</h3>
					</div>
					<div v-show="d1" @click="selectD1()" class="col-xs-1 col-sm-1 col-md-1 data-tabs" id="tab-0" :class="{f_tab: !tab_0_selected}">
						<h3 class="text-center">القسم الاول</h3>
					</div>
				</div>

			</div>
			<div class="row">
				<img id="img-bar" src="/images/img-bar.png">
			</div>

			<div class="tab-data-container">
				<div v-for="team in table" class="row table-content">
					<div class="col-xs-12 col-sm-12 col-md-12" :class="{'second':$index%2==0, 'first':$index%2==1}">
						<div class="table-data-content">
							<div class="row">
								<div class="col-xs-11 col-sm-11 col-md-11">
									<p class="text-center table-data-division">@{{team}}</p>
								</div>
								<div class="col-xs-1 col-sm-1 col-md-1">
									<p class="table-data-number">@{{$index+1}}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

	
			
		</div>
	</div>

</div>
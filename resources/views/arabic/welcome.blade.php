 <!DOCTYPE html>
<html>
    <head>
        <title>FLB Website</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">


        <link rel="stylesheet" type="text/css" href="/ar/css/home.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/about.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/directors.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/committees.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/purpose.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/mission.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/division.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/teams.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/calendar.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/media.css">
        <link rel="stylesheet" type="text/css" href="/ar/css/contact.css">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.5.0/fullcalendar.min.css">



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.0/moment.min.js"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.5.0/fullcalendar.min.js"></script>
        <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <link rel="stylesheet" href="/css/bootstrap-image-gallery.min.css">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                @include('arabic.home')
            </div>
            <div class="row">
                @include('arabic.about')
            </div>
            <div class="row">
                @include('arabic.directors')
            </div>
            <div class="row">
                @include('arabic.purpose')
            </div>
            <div class="row">
                @include('arabic.mission')
            </div>
            <div class="row">
                @include('arabic.divisions')
            </div>
            <div class="row">
                @include('arabic.teams')
            </div>
            <div class="row">
                <img src="/images/img.jpg">
            </div>
            <div class="row">
                @include('arabic.calendar')
            </div>
            <div clas="row">
                @include('arabic.media')
            </div>
            <div class="row">
                @include('arabic.contacts')
            </div>
            <div class="row">
        
                <div class="row">
                    <div id="footer">
                        © copyright 2015 flbb.com | Designed & Developed by Stroberry Advertising
                    </div>
                </div>
            </div>
        </div>




        <script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
        <script src="/js/bootstrap-image-gallery.min.js"></script>
        <script type="text/javascript" src="http://cdn.jsdelivr.net/vue/1.0.13/vue.js"></script>
        <script type="text/javascript" src="/ar/js/committees.js"></script>
        <script type="text/javascript" src="/ar/js/directors.js"></script>
        <script type="text/javascript" src="/ar/js/divisions.js"></script>
        <script type="text/javascript" src="/js/media.js"></script>
        <script type="text/javascript" src="/ar/js/teams.js"></script>


    </body>

    <script type="text/javascript">
        var width = $(window).width();
        var factor = 0.05234375;
        var zoom = width*factor;
        zoom = zoom.toString();
        zoom = zoom+"%";
        document.body.style.zoom=zoom;

        $( window ).resize(function() {
            var width = $(window).width();
            var factor = 0.05234375;
            var zoom = width*factor;
            zoom = zoom.toString();
            zoom = zoom+"%";
            document.body.style.zoom=zoom;
            document.body.style.zoom=zoom;
        });

    </script>
</html>

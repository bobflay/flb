<div id="home-container">
		<div class="row">
			<div id="menu">
			</div>
			<div id="menu-container">
					<div  id="menu-items">

						<div id="first-m" class="menu-item-container white col-xs-1 col-sm-1 col-md-1">
							<p class="text-center menu-item">
								<strong>
									<span><a href="/">إنكليزي</a></span> / <span><a href="/arabic">عربي</a></span>
								</strong>
							</p>
						</div>


						<div id="second-m" class="menu-item-container col-xs-1 col-sm-1 col-md-1 apple-icon">
							<div class="facebook-ico">
								<p class="text-center menu-item">
									<img id="apple-logo" src="/images/apple.png">
								</p>
							</div>
						</div>

						<div id="third-m" class="menu-item-container col-xs-1 col-sm-1 col-md-1">
							<p class="text-center menu-item">
								<a class="menu-link" href="#calendar-container"><strong>الروزنامة</strong></a>
							</p>
						</div>

						<div id="fourth-m" class="menu-item-container col-xs-1 col-sm-1 col-md-1">
							<a href="#divisions-container">
								<p class="text-center menu-item">
									<strong>الأخبار</strong>
								</p>
							</a>
						</div>

						<div id="fifth-m" class="menu-item-container col-xs-1 col-sm-1 col-md-1">

							<a href="#teams-container">
								<p clas="text-center menu-item">
									<strong>
										الفرق الوطنية
									</strong>
								</p>
								
							</a>
							
						</div>

						<div id="sixth-m" class="menu-item-container col-xs-1 col-sm-1 col-md-1">
								<a href="#menu">
									<p class="text-center menu-item">
										<strong>
											الاستراتيجية
										</strong>
									</p>

								</a>		
						</div>


						<div id="seventh-m" class="menu-item-container col-xs-1 col-sm-1 col-md-1">
								<a href="#divisions-container">
									<p class="text-center menu-item">
										<strong>
											النوادي والانقسامات
										</strong>
									</p>
								</a>		
						</div>


						<div id="eight-m" class="menu-item-container col-xs-1 col-sm-1 col-md-1">
								<a href="#about-container">
									<p class="text-center menu-item">
										<strong>
											الاتحاد
										</strong>
									</p>

								</a>
								
						</div>

						<div id="nineth-m" class="menu-item-container white col-xs-1 col-sm-1 col-md-1" id="home-ar">
								<a href="#menu">
									<p class="text-center menu-item">

										<strong>
											الرئيسة
										</strong>
									</p>
								</a>
						</div>





					</div>
			</div>
		</div>
		
		<div class="row" id="home-messages">
			<div id="general">
				<div id="first-container" class="pull-right">
					<div class="row">
						<div id="president-message" class="col-xs-4 col-sm-4 col-md-4 pull-right">
							<div >
								<img src="/ar/images/president-msg-bg-ar.png" id="president-message-img">
								<div id="president-text">
									<div id="president-scroll">
										<p id="president-txt" class="text-justify">
											غريب أمر هذه اللعبة كيف سكنت قلوب اللبنانيين وكم صفّقت لها الأيادي وأدمعت لها العيون. 

											عرفت العصور الذهبية وبقيت خيوطها ذهبية لمّاعة من خلال كوكبة من الإداريين ونخبة من اللاعبين...

											وكم دخلت في مراحل تماوج فيها الفرح والفوز مع تقبّل عكس ذلك. 

											واندفاع المستثمرين وحماس الجماهير.

											أيًّا تكن المواقف والمواقع، تبقى كرة السلة خارجة عن الزمان لتستمر وتنتصر على الأيام. 

											لقد كُتب لهذه اللعبة أن ترعى شباب لبنان وجمهور لبنان وتحتضنهما، وبإمكانها أن تحرّك الشارع اللبناني.

											عرفت هذه اللعبة امجادًا وانتصارات، ونحن متأكّدون من أنّنا سنظلّ أوفياء لها، وأمناء على تراثها، جديرين 

											بطريقة عجز عنها الكثيرون...

											بانتصاراتها، وعاملين من أجل الحفاظ عليها.

											رئيس الاتحاد اللبناني لكرة السلة
																				
										</p>
										
										</div>
								</div>
							</div>
						</div>
						<div class="col-xs-3 col-sm-3 col-md-3 pull-right" id="news-container">
							<div>
								<img src="/ar/images/new-bg-ar.png" id="news-img">
								<div id="news-data-container">

									<div class="news-element">
										<h3 class="text-justify">
											مؤتمر صحافي حاشد لاتحاد كرة السلة
											نصّار: نجحنا في كسب ثقة المستثمر 
											افرام: من شير جونية إلى الريو دي جينيرو
										</h3>
										<p class="news-body text-justify">
											إلى القسم الرياضي المحترم<br>

											  تحت شعار "شوف حالك بلبنان"، أعلن الاتحاد اللبناني لكرة السلّة عن أسماء بعثة منتخب لبنان للرجال المشاركة في بطولة 

												غرب آسيا في الأردن في الفترة الممتدة من 29 أيار الجاري ولغاية 2 حزيران المقبل والمؤهّلة إلى بطولة آسيا، وعن الرعاية 

												شركة "سانيتا" الرسمية للمنتخب، وذلك خلال مؤتمر صحافي حاشد عُقد في مطعم "لا كريبري". تقدّم الحضور وزير 

												الشباب  والرياضة العميد عبدالمطلب حنّاوي، رئيس لجنة الشباب والرياضة البرلمانية النائب سيمون أبي رميا، رئيس اتحاد 

												كرة السلة المهندس وليد نصّار وأعضاء الاتحاد، الرئيس التنفيذي لمجموعة "إندفكو" نعمة افرام ومسؤولو شركة "سانيتا" 

												التابعة للمجموعة، أمين عام اتحاد غرب آسيا لكرة السلة جان تابت، رؤساء أندية، المحاضر الأولمبي الدولي جهاد سلامة، 

												الجهاز الفنّي والطبّي والإداري ولاعبو منتخب لبنان وحشد كبير من رجال الصحافة والإعلام.
										</p>

										<a href="https://drive.google.com/file/d/0BypjlWKMZEkuUGUxSkZkU1lHWHM/view?usp=sharing" target="_blank" class="btn btn-success" >المزيد</a>
									</div>

									<div class="news-element">
										<h3 class="text-justify">
											بطولة آسيا في كرة السلة

											لبنان في المجموعة الرابعة

											مع تايوان وقطر وكازاخستان
										</h3>
										<p class="news-body text-justify">
											أوقعت القرعة منتخب لبنان في كرة السلة للرجال ضمن المجموعة الرابعة لبطولة الأمم الآسيوية التي ستستضيفها الصين بين 

											23 أيلول و3 تشرين الأول المقبلين. وسيتأهل الفائز في البطولة إلى أولمبياد ريو دي جينيرو الصيفي في البرازيل العام المقبل.

											وفي ما يلي نتيجة القرعة حيث قسمّت المنتخبات إلى أربع مجموعات، تضمّ كل مجموعة أربعة منتخبات:<br>

											- المجموعة الأولى: إيران (بطلة آسيا للعام 2013)، اليابان، ماليزيا، منتخب من جنوب آسيا.<br>

											- المجموعة الثانية: الفيليبين، فلسطين، الكويت، منتخب من شرق آسيا.<br>

											- المجموعة الثالثة: كوريا الجنوبية، الأردن، سنغافورة، الصين.<br>

											- المجموعة الرابعة: تايوان، لبنان، قطر، كازاخستان.


										</p>
									</div>

									<div class="news-element">
										<h3 class="text-justify">
											بيان شكر من رئيس اتحاد كرة السلة
										</h3>
										<p class="news-body text-justify">
											إلى القسم الرياضي المحترم<br>

											شكر رئيس الاتحاد اللبناني لكرة السلة المهندس وليد نصار كل من ساهم في إنجاح التظاهرة الرياضية المتمثّلة باللقاء الذي 

											جرى بين ناديي الحكمة والرياضي على ملعب نادي غزير الأحد الفائت. وجاء في بيان أصدره رئيس الاتحاد: "أود أن أشكر 

											كل من ساهم في إنجاح لقاء الحكمة والرياضي، ولا سيّما أعضاء اللجنة الإدارية والأمانة العامة ولجنة إدارة البطولة ولجنة 

											الملاعب، على التنظيم الإداري واللوجستي المميّز الذي لم يَشُبه أي شائبة تذكر.
										</p>
										<a href="https://drive.google.com/file/d/0BypjlWKMZEkuRVZ0ZTBmcGNOUFE/view?usp=sharing" target="_blank" class="btn btn-success" >المزيد</a>
									</div>

									<div class="news-element">
										<h3 class="text-justify">
											نادي بيبلوس بطل كأس السوبر
										</h3>
										<p class="news-body text-justify">
											حقّق نادي بيبلوس «ثلاثية تاريخية» في كرة السلة إذ أحرز لقبين رسميين ولقبًا ودّيًا في أقل من شهر؛ فبعدما أحرز لقب 

											دورة هنري شلهوب الودّية بفوزه على التضامن (زوق مكايل) في النهائي، ولقب مسابقة كأس لبنان بفوزه على الحكمة، أضاف 

											النادي الجبيلي لقبًا لا يقلّ شأنًا عن لقبيه الأولين، بإحرازه لقب كأس السوبر على كأس الراحل أنطوان شارتييه في المباراة التي 

											جمعت بطل لبنان الرياضي وحامل لقب كأس لبنان بيبلوس.

											المزيد
										</p>
										<a href="https://drive.google.com/file/d/0BypjlWKMZEkuWTRHV2ZFQ0NoYU0/view?usp=sharing" target="_blank" class="btn btn-success">
											المزيد
										</a>
									</div>
								</div>


								</div>

							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>

</div>
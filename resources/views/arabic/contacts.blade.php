<div id="contacts-container">

		<div class="pull-right row calendar-title contacts-title">
			<div class="col-md-6 col-md-offset-6 pull-right">
				<p id="directors-title1">
					<strong>[اتصل]</strong>
				</p>
				<p  class="directors-title2-contacts">
					<strong>بنا</strong>
				</p>
			</div>
		</div>
		<div class="row white" id="contacts-data-row">
			
			<div class="col-md-6">
				<div id="mail-form-container">
					<div class="row">
						<img src="/ar/images/contacts-us-arabic.png">
					</div>
					<div class="row">
						<input type="text" class="form-padding" name="name" placeholder="الاسم" id="name-txt">
					</div>
					<div class="row">
						<input type="text" class="form-padding" name="email" placeholder="البريد الالكتروني" id="email-txt">
					</div>
					<div class="row">
						<textarea type="text" class="form-padding" name="message" placeholder="الرسالة" id="message-txt"></textarea>
					</div>

					<div class="row" id="submit-row">
						<div class="">
							<img src="/ar/images/send-btn-ar.png">
						</div>
					</div>

				</div>
			</div>

			<div class="col-md-6">
				<div class="row">
					<p id="p1" class="contacts-width grey2 text-right">
						فريق العمل لدينا مكرّس تماما للرد على أي من الأسئلة الخاصة بك أو الاستفسارات. لا تتردد في الاتصال بنا، أو استخدام النموذج أدناه لإرسال رسالتك.
					</p>
				</div>
				<div id="p2" class="row pull-right">

					<div class="row">
						<div class="col-md-4 red">
							+ ٩٦١ ٤ ٧٢٢ ٠٢٣
						</div>
						<div class="col-md-2">
							الهاتف
						</div>
					</div>
					<div class="row">
						
						<div class="col-md-4 red">
							+ ٩٦١ ٤ ٧٢٢ ٠٢٥ 
						</div>
						<div class="col-md-2">
							فاكس
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 red">
							info@flbb.com
						</div>
						<div class="col-md-2" style="width:200px;">
							البريد الالكتروني
						</div>
					</div>

				</div>
				<div class="row pull-right">
					<p id="p3" class="contacts-width grey2 text-right">
						,وايت هاوس أوتيل بلوك ب<br>
						الطابق الثالث و الرابع, جل الديب
					</p>
				</div>

			</div>
			
		</div>


</div>
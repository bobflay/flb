<div id="mission-container">
	<div class="row data-inner">
		<div class="top-bar top-bar-p">
		<div class="col-xs-6 col-sm-6 col-md-6  col-xs-offset-6 col-sm-offset-6 col-md-offset-6 text-right">
				<p id="mission-title1">
					<strong>[مهام]</strong>
				</p>
				<p id="mission-title2">
					<strong>الاتحاد</strong>
				</p>
			</div>
		</div>
	</div>

	

	<div class="images images-p">
		<div class="row">

			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/basket.png">
					<div class="user-data">
						<p class="text-center p-title-m"><strong>البطولات المحلية</strong></p>
						<p class="text-center p-body-m">السماح للجمعيات بعقد البطولات المحلية الخاصة والمشاركة في البطولات الخارجية.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/board.png">
					<div class="user-data">
						<p class="text-center p-title-m"><strong>نشر القوانين والقرارات</strong></p>
						<p class="text-center p-body-m">نشر القوانين والقرارات، وإصدار المنشورات الرسمية للاتحاد.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/siren.png">
					<div class="user-data">
						<p class="text-center p-title-m"><strong>الإشراف على البطولات</strong></p>
						<p class="text-center p-body-m">الإشراف على البطولات التي تقام على الأراضي اللبنانية والمشاركة في البطولات والمباريات الدولية خارج لبنان.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/cup.png">
					<div class="user-data">
						<p class="text-center p-title-m"><strong>تنظيم البطولات السنوية وعقدها</strong></p>
						<p class="text-center p-body-m">تنظيم وعقد الدورات السنوية لمختلف الدرجات والفئات العمرية، للرجال والنساء، وتنظيم دورات للمدرّبين، وإعداد دورات للحكّام.</p>
					</div>
				</div>
			</div>
			
			
			
		</div>
	</div>

</div>
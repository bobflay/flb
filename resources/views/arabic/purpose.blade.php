<div id="purpose-container">
	<div class="row data-inner">
		<div class="top-bar top-bar-p">
		<div class="col-xs-6 col-sm-6 col-md-6 col-xs-offset-6 col-sm-offset-6 col-md-offset-6 text-right">
				<p id="purpose-title1">
					<strong>[الهدف من ]</strong>
				</p>
				<p id="purpose-title2">
					<strong>الاتحاد</strong>
				</p>
			</div>
		</div>
	</div>

	<div class="images images-p">
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/perp.png">
					<div class="user-data">
						<p class="text-center p-title"><strong>الحفاظ على الوجود</strong></p>
						<p class="text-center p-body">الحفاظ على وجود الاتحاد باعتباره السلطة المدنية والقانونية الحصرية والشرعية لإدارة المباريات في لبنان.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/hands.png">
					<div class="user-data">
						<p class="text-center p-title"><strong>خلق علاقات ودّية</strong></p>
						<p class="text-center p-body">خلق علاقات ودّية مع الاتحاد الدولي للعبة والاتحاد الآسيوي والاتحاد العربي واتحاد غرب آسيا ومع الاتحاد من اتحادات الدول، وفقًا لبقية الاتحادات الدولية والقارية والعربية التي تشرف على اللعبة.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/t-shirt.png">
					<div class="user-data">
						<p class="text-center p-title"><strong>العمل على توثيق العلاقات</strong></p>
						<p class="text-center p-body">العمل على توثيق العلاقات بين جمعيات الاتحاد.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/ball.png">
					<div class="user-data">
						<p class="text-center p-title"><strong>الإشراف على مباريات كرة السلة</strong></p>
						<p class="text-center p-body">الإشراف على مباريات كرة السلة، وتنظيم البطولات وتطوير كرة السلة في لبنان، وهو من يمثّل هذه الرياضة في الخارج ويدعم المواهب الشابّة.</p>
					</div>
				</div>
			</div>
			
			



		</div>
	</div>

</div>
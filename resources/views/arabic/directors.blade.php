<div id="directors-container">
	<div class="row data-inner">
		<div class="top-bar">

			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="controls-container">
					<img class="control-left" @click="next()" src="/images/left.png">
					<img class="control-right" @click="back()" src="/images/right.png">
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div id="directors-title3" class="pull-right">
					<p id="directors-title1">
						<strong>[مجلس الادارة]</strong>
					</p>
					<p id="directors-title2">
						<strong>المدراء والأعضاء</strong>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="images">
		<div class="row">
			<div v-show="img_3" class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="@{{img_3.image}}">
					<div class="user-data">
						<p class="user-name">@{{img_3.name}}</p>
						<p class="user-title text-right">[@{{img_3.position}}]</p>
					</div>
				</div>
			</div>
			<div v-show="img_2" class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="@{{img_2.image}}">
					<div class="user-data">
						<p class="user-name">@{{img_2.name}}</p>
						<p class="user-title text-right">[@{{img_2.position}}]</p>
					</div>
				</div>
			</div>
			<div v-show="img_1" class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="@{{img_1.image}}">
					<div class="user-data">
						<p class="user-name">@{{img_1.name}}</p>
						<p class="user-title text-right">[@{{img_1.position}}]</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="@{{img_0.image}}">
					<div class="user-data">
						<p class="user-name">@{{img_0.name}}</p>
						<p class="user-title text-right">[@{{img_0.position}}]</p>
					</div>
				</div>
			</div>
			
			
			
		</div>
	</div>

</div>
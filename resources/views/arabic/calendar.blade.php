<div id="calendar-container">
	<div class="row">
		<div class="calendar-title pull-right">
			<div class="col-md-offset-6 col-md-6">
				<p class="black">
					<strong>[الجدول الزمني و]</strong>
				</p>
				<p id="directors-title2">
					<strong>التقويم</strong>
				</p>
			</div>
		</div>
	</div>
	<br>
	<br>
	<br>

	<div class="row">

		<div class="col-md-6">
		</div>

		<div class="col-md-6">
			<div id='calendar'></div>
		</div>

		
</div>


<script type="text/javascript">
	$(document).ready(function() {

	    // page is now ready, initialize the calendar...

	    $('#calendar').fullCalendar({
  		events: [
	         {
	            title  : 'event1',
	            start  : '2016-01-05',
	            end    : '2016-01-05'
	        },
	        {
	            title  : 'event2',
	            start  : '2016-01-06',
	            end    : '2016-01-06'
	        },
	        
    	]	    

		})

	});
</script>
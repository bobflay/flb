<div id="purpose-container">
	<div class="row data-inner">
		<div class="top-bar top-bar-p">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<p id="purpose-title1">
					<strong>[The purpose of the]</strong>
				</p>
				<p id="purpose-title2">
					<strong>FEDERATION</strong>
				</p>
			</div>
		</div>
	</div>

	<div class="images images-p">
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/ball.png">
					<div class="user-data">
						<p class="text-center p-title"><strong>SUPERVISING THE BASKETBALL GAMES</strong></p>
						<p class="text-center p-body">Supervising the basketball games, organizing the tournaments, developing Basketball in Lebanon, representing the sport abroad and supporting young talents.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/t-shirt.png">
					<div class="user-data">
						<p class="text-center p-title"><strong>WORKING ON CLOSER TIES</strong></p>
						<p class="text-center p-body">Working on closer ties between the acceding associations.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/hands.png">
					<div class="user-data">
						<p class="text-center p-title"><strong>CREATING FRIENDLY RELATIONSHIPS</strong></p>
						<p class="text-center p-body">Creating friendly relationships with the International Federation of the game, the AFC, the Arab Union, the West Asia Union and with the Union of Federations of States, acceding to the rest of the international, continental and Arab Unions, which oversee the game.</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="/images/perp.png">
					<div class="user-data">
						<p class="text-center p-title"><strong>MAINTAINING THE PRESENCE</strong></p>
						<p class="text-center p-body">Maintaining the presence of the Federation as the exclusive civil and legal authority & legitimacy for the management of the games in Lebanon.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
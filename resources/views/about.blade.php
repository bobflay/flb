<div id="about-container">
	<div class="about-data-container">

		<div id="league-container" class="row">
			<div class="col-xs-6">
				<div id="about-div">
					<p class="about-section">
						<strong>[about]</strong>
					</p>
					<p class="about-title">
						<strong>THE FEDERATION</strong>
					</p>
					<div class="about-body">
						<p id="first-p" class="text-justify about-body">
							The Lebanese Basketball Federation saw the light at the beginning of sixties, after its separation from the Federation of volleyball, and has gained its own entity ever since. Many successions for the Federation’s presidency took place, which later led to a lot of achievements. The presidential states were not for long periods of time, until 1970 when Toni Khoury was elected president of the Federation and his presidency lasted 26 years, which is the longest so far. Khoury left the presidency in 1996 and Antoine Chartier (coming from the world of volleyball) was elected president in the fall of that year until 1999.
							<br>
							<br>
							In the late of October 1999, Jean Hammam (also coming from the world of volleyball) was elected President of the Federation, and his presidency lasted five years, from 1999 till 2004. In 2005, Michel Tannous was elected president, his presidency continued for nearly two years and a half (2005-2007). Then Pierre Kakhia was elected (2007 - 2010) followed by George Barakat (August 2010-2012) and Dr. Robert Abu Abdullah (from the end of 2012 till the autumn of 2013) followed by Engineer Walid Nassar (a former basketball player) in the famous elections which were held on 21 December 2013, at the Central School of Jounieh, with 15 members of the Federation present. The mandate of the current Federation presidency expires in 2016, and will be followed by a general election for all sports federations and for the Executive Committee of the Olympics, following the Rio de Janeiro Olympics - a system that has been adopted by the International Olympic Committee for several years.
						</p>
					</div>
					
				</div>
			</div>
			<div class="col-xs-6">
				<div id="rules-div">
					<p class="about-section">
						<strong>[general]</strong>
					</p>
					<p class="about-title">
						<strong>STATUTE</strong>
					</p>
					<div class="about-body">
						<ul id="rules-list">
							<li class="rules">
								The Lebanese Basketball Federation is performing in Lebanon by virtue of 

								notified announcement issued by the Ministry of National Education and 

								Fine Arts, under the number 3039 on the date of September 03, 1952, it is 

								a Federation of Associations duly authorized to practice the basketball 

								game in Lebanon, and similar federation shall not be established in the 

								Republic of Lebanon, and it is a member of the International, Asian and 

								Arab Federations of such game and in the Lebanese Olympic Committee, it 

								is committed to their statutes and rules and it is performing in total 

								independency and apart from any interference, and it is performing with no 

								distinction and in absolute impartiality away from any belonging or 

								affiliation. At the same time it is maintaining its presence as a civil authority 

								not governmental which is not seeking for any gains, it is performing 

								legally, legitimately and alone in order to manage the Basketball game in 

								Lebanon and to ensure its independency and all the authorities in order to 

								set and respect the rules of the game.
							</li>
							<li class="rules">
								The location of the Federation shall be in Beirut or its suburbs and its board

								of directors determined the official location of the Federation on the 

								following address: 

								Jal-el-Dib Highway – White House Building – Block (B) – Both Third and 

								Fourth Floors.
							</li>
							<li class="rules">
								Aims of the Federation shall be: 
								<br>
								1. Supervising the basketball game in Lebanon as for its 
								<br>
								2. Binding and consolidating the relations between the Federation 
								<br>
								3. Binding and consolidating the relations with the local official 
								<br>
								4. Binding and consolidating the relations with the Federations of the 
								<br>
								5. Permanent relentless pursuing to banish politics, religion, personal 
								<br>
								management, organization of its championships, its representation 

								abroad, its spreading and its development. 

								and the Clubs affiliated thereto, as well between the Clubs 

								themselves. 

								competent authorities, with the rest of the Sportive Federations 

								adhered under the flag of the Lebanese Olympic Committee. 

								affiliated country to the International, Continental, Provincial and 

								Arab Federations that are supervising this game 

								interests and personal gains, to work with democracy and 

								transparency with no distinction between race and sex and to 

								adopt equity and fairness.
							</li>
							<li class="rules">
								<a href="/flb.pdf" target="_blank" class="btn btn-success" > Download the full PDF</a>
							</li>
							
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div id="achievements-container" class="row">
			<div class="col-xs-6">
				<div id="team-div">
					<p class="about-section">
						<strong>[team]</strong>
					</p>
					<p class="about-title">
						<strong>ACHIEVEMENTS</strong>
					</p>
					<p id="achievements-p" class="text-justify about-body">
						Everyone agrees that Lebanese Basketball was marked by many external accomplishments at the level of teams and clubs.
						<br>
						<br>
						The new era of the Lebanese Basketball started in 1993 with the adoption of the foreign players system and the final-four system (semi-finals). The final series adapted by the Federation was the idea of the new president of Sagesse club Mr Antoine Choueiry. 
						<br>
						<br>
						At the level of teams, the Lebanese men team was qualified for the World Championship finals, after coming second in the Asian Championship, held in July 2001 in the Chinese city of Shanghai. This achievement is considered the first at the level of Lebanese collective games. The Lebanese team then participated in the World Championship that took place in Indianapolis in 2002, under the era of President Jean Hammam.
						<br>
						<br>
						In 2005, the Cedars team repeated its achievement, since it occupied the runner-up position in the Asian Championship, which took place in Doha (Qatar) under the era of President Michel Tannous, and it qualified for the world finals championships, which took place in Japan in 2006
					</p>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div id="clubs-div">
					<p class="about-section">
						<strong>[club]</strong>
					</p>
					<p class="about-title">
						<strong>ACHIEVEMENTS</strong>
					</p>
					<p id="clubs-p" class="text-justify about-body">
						- The general assembly held a meeting on December 21, 2013 at the Central 

College - Jounieh auditorium to ratify the administrative and financial statements 

for the season 2012-2013, and to elect a new managing committee. The posts were 

distributed unanimously as follows:

President (Chairman): Walid Nassar  

Vice President: Fares Modawwar

Second Vice President: Nizar Al Rawwas

Third Vice President: Nader Basma

Fourth Vice President: Viken Djerejian

Secretary-General: attorney Ghassan Fares

Treasurer: Elie Farhat

Accountant: George Sabounjian

National Teams Director: Nazih Bouji

Advisory Members: Fadi Tabet 

Maroun Gebrayel

Hadi Ghemrawi 

Rami Fawwaz 

Roger Achqouti 

Fouad Saliba
<br>
- The Lebanese Basketball Federation (FLB) members visited his Excellency the 

Minister of Youth and Sports Faysal Karami on January 21, 2014.
<br>
- FLB president Engineer Walid Nassar visited the International Basketball 

Federation (FIBA) between 1 and 3 April 2014, to speed up the process of lifting 

the ban off Lebanon.
<br>
- FLB brought in the technical supervisor Stelios Koukoukilidis to oversee the 

referees from April 1, 2014 till the tournament’s end.
<br>
-  A consultative meeting was held at the Antranik Club headquarters, attended by 

the federation members; administrative committee of the 1st division league; 

Objection & Appeal committee; presidents, representatives, and coaches of the 

first-degree clubs; games’ supervisors; international referees; and Greek 

international supervisor.
<br>
- The federation held an exceptional general assembly at the Antranik Club 

headquarters on April the 16th to amend the general rules according to FIBA 

recommendations.
<br>
- FIBA lift the ban off Lebanon on May 7, 2014.
<br>
- Federation’s president Eng Walid Nassar, signed on the major headlines for a 

sports cooperation protocol with the UAE Basketball Association.
<br>
- Vice president Fares Moudawwar participated in the proceedings of the general 

assembly for the West Asian Basketball Association (WABA) on May 18, 2014.
<br>
- FLB members visited the Youth and Sports Minister Brigadier General Abdul 

Muttalib Hinnawi on May 27, 2014.
<br>
- The two youngsters Jad Khalil and Ahmad Sbeity participated in the Basketball 

without Borders Asia Camp from the 13th to the 16th of June 2014 in Chinese 

Taipei.
<br>
- Mr. Benjamin Cohen, FIBA legal affairs director, attended the Lebanese 1st 

division Men Championship finals.
<br>
- FLB members visited the Youth and Sports Minister Brigadier General Abdul 

Muttalib Hinnawi on July 14, 2014. 
<br>
- The federation’s member Fadi Tabet participated in the FIBA electoral general 

assembly on July 24, 2014.
<br>
- Coaches Charbel Albesh and George Khalid participated in a trainers’ course in 

Dubai from August 14 to18, 2014.
<br>
- International referee Walid Abi Rached participated in the Asian Championship 

Under-18 years as a neutral referee from August 19 till 29 in Qatar.
<br>
- Federation’s president Eng. Walid Nassar participated in the proceedings of the 

general assembly for FIBA August 28 & 29, 2014 in Spain.
<br>
- Federation’s president Eng. Walid Nassar was appointed as championships 

committee member in FIBA on September 17, 2014.
<br>
- FLB organized its first workshop at the Antranik Club on September 28, 2014.
<br>
- Second vice president Nizar Rawwas participated in the proceedings of the 

general assembly for the Arab Basketball Association ABA on November 4 & 5, 

2014 in Morocco.
<br>
- FLB organized a refining course “Referee Clinic” for the new referees. The 

graduating ceremony was held at the Central Jounieh auditorium on November 

14, 2014.
<br>
- Two FLB members, Fouad Saliba and Fadi Tabet, were appointed in the technical 

committee and the championships committee of WABA.
<br>
- Former President Antoine Chartier passed away on the 15th of December 2014. All 

activities were suspended in respect to his soul and for his mark in the sports 

world, especially in basketball.
<br>
- The general assembly was held on March 15, 2015 at the Central College – 

Jounieh auditorium, to ratify the administrative and financial statements for the 

season 2013-2014.
<br>
- FLB organized refining course for coaches “Coaching Clinic” (age groups) at the 

Central College – Jounieh from March 27 till 29, 2015 under the supervision of 

ABA and WABA, as well as the Lebanese Ministry of Youth and Sports. 23 

participants out of 47 successfully passed.
<br>
- FLB member Fouad Saliba participated in the 3x3 Global Congress on June 6 & 

7, 2015 in Hungary.
<br>
- International referee Rabah Njeim participated, as a neutral referee, in the World 

Championship Under-19 years from June 27 till July 5, 2015 in Greece.
<br>
- International referee Paul Soukaim participated, as a neutral referee, in the 

European Championship Under-20 years from July 9 till 19, 2015 in Hungary.
<br>
- The Asian basketball federation organized within its FIBA Asia Development 

Plan, a management seminar in Lebanon for the West Asian countries, from May 

22 till 24, 2015. Secretary-generals and directors of the federations in West Asia – 

including Lebanon – attended, and international lecturers spoke.
<br>
- FLB announced during a press conference on the 27th of May that Sanita would 

sponsor the National Team.
<br>
- FLB participated in the Olympic Day organized by the Lebanese Olympic 

Committee on May 31, 2015 at the Fouad Chehab Stadium.
<br>
- Secretary-general attorney Ghassan Fares participated in the proceedings of the 

general assembly for the Arab Basketball Association (ABA) in Dubai on 

November 12 & 13, 2015. 
<br>
- Arab Basketball Association organized a Referee Clinic for the Lebanese referees 

from November 13 till 15 at the Central College-Jounieh with the participation of 

the lecturer Mr. Moder Majzoub.
					</p>
				</div>
			</div>
		</div>

	</div>

</div>
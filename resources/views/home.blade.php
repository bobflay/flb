<div id="home-container">
		<div class="row">
			<div id="menu">
			</div>
			<div id="menu-container">
					<div  id="menu-items">
						<div id="first-home" class="menu-item-container col-sm-1 col-md-1">
							<p  class="text-center menu-item">
								<a href="#menu" class="menu-link">
									<strong>HOME</strong>
								</a>
							</p>
						</div>

						<div class="menu-item-container col-sm-1 col-md-1">
							<p class="text-center menu-item">
								<a class="menu-link" href="#about-container"><strong>THE FEDERATION</strong></a>
							</p>
						</div>

						<div class="menu-item-container col-sm-1 col-md-1">
							<a href="#divisions-container">
								<p class='text-center menu-item'>
									<strong>CLUBS & DIVISIONS</strong>
								</p>
							</a>
						</div>

						<div class="menu-item-container col-sm-1 col-md-1">
							<a href="#teams-container">
								<p class="text-center">
									<strong>
										NATIONAL TEAMS
									</strong>
								</p>

							</a>	
						</div>

						<div class="menu-item-container col-sm-1 col-md-1">
							<p class="text-center menu-item">
								<a href="#calendar-container">
									<strong>
										CALENDAR
									</strong>
								</a>
								
							</p>
						</div>

						<div class="menu-item-container col-sm-1 col-md-1">
							<p class="text-center menu-item">
								<a href="#media-container">
									<strong>
										MEDIA ROOM
									</strong>
								</a>
								
							</p>
						</div>

						<div class="menu-item-container col-sm-1 col-md-1">
							<p class="text-center menu-item">
								<a href="#contacts-container">
									<strong>
										CONTACTS
									</strong>
								</a>
								
							</p>
						</div>

						<div class="menu-item-container col-sm-1 col-md-1 apple-item">
							<div class="row">
								<div class="">
									<div class="facebook-ico">
										<p class="text-center menu-item">
											<img id="apple-logo" src="/images/apple.png">
										</p>
									</div>
								</div>

							</div>
						</div>

						<div class="menu-item-container white col-sm-1 col-md-1">
								<p class="text-center menu-item">
									<strong>
										<span><a href="/">EN</a></span> / <span><a href="/arabic">AR</a></span>
									</strong>
								</p>
						</div>
					</div>
			</div>
		</div>
		
		<div class="row">
			<div id="general">
				<div class="row">
					<div id="president-message" class="col-xs-4 col-sm-4 col-md-4">
						<div >
							<img src="/images/president.png" id="president-message-img">
							<div id="president-text">
								<div id="president-scroll">
									<p id="president-txt" class="text-justify">
										It is inexplicable how basketball has occupied the Lebanese people’s hearts, the amount of 

										applause it has received, and the tears that has been shed.

										It went through stages of joy and victories, and at times losses just like life, which is filled with 

										tears and smiles.

										Basketball in Lebanon experienced the golden ages through a constellation of administrators, a 

										group of players, the impulse of investors, and the enthusiasm of the crowd.

										Whatever the situation is, basketball is a timeless game and will continue to strive.

										This game is meant to embrace the Lebanese youth and people.  It can move the Lebanese people 

										in ways no one else can. 

										This game experienced glorious moments, and we will always be faithful to its heritage, victory, 

										and legacy.
									</p>
									
									</div>
							</div>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3" id="news-container">
						<div>
							<img src="/images/news-container.png" id="news-img">
							<div id="news-data-container">

								<div id=" " class="news-element text-justify">

									<h3>Thankful Declaration from the president of the basketball federation to the honorable Sirs in the sport division
									</h3>

									<p class="news-body text-justify">
										The president of Lebanese Basketball Federation, engineer Walid Nassar, thanked all those who 

										contributed to the success of the sport event that gathered Sagesse and Sporting clubs on the 

										Ghazir Stadium last Sunday. The declaration of the president contained: “I would like to thank 

										all those who contributed to the success of the game between Sagesse and Sporting club, 

										especially the members of the management committee, the secretariat, the managerial committee 

										of the tournament, and the stadium committee for the outstanding managerial and logistical role 

										played that resulted in avoiding the happening of any memorable incident”.
									</p>

									<a href="https://drive.google.com/file/d/0BypjlWKMZEkuX3RGOExaOTczdjA/view?usp=sharing" target="_blank" class="btn btn-success" >more</a>
								</div>

								<div id=" " class="news-element text-justify">

									<h3>Asian Basketball Championship
										Lebanon in the fourth group
										With Taiwan, Qatar, and Kazakhstan
									</h3>

									<p class="news-body text-justify">
										The drawing lots left the Lebanese basketball team for men within the fourth group for Asian championship, which will be hosted by China, between the coming 23rd of September and the 3rd of October. The winning team will be qualified to the Rio de Janeiro summer Brazilian Olympics next year.
										In the following is the result of the draw where teams were divided into four groups and each group contains four teams:
											•	The first group: Iran (Asian champion of 2013), Japan, Malaysia and South Asia’s teams.
											•	The second group: Philippines, Palestine, Kuwait and East Asia’s teams.
											•	The third group: South Korea, Jordan, Singapore and china’s teams.
											•	The fourth group: Taiwan, Lebanon, Qatar and Kazakhstan’s teams.
									</p>
								</div>

								<div id="" class="news-element text-justify">

									<h3>
										To the esteemed sports section,
									</h3>

									<p class="news-body text-justify">
										Under the slogan “Be Proud of Lebanon”, FLB announced the members of the Men’s 

										Lebanese National Basketball Team participating in the West Asian Championship 

										from May 29 till June 2 in Jordan, which qualifies to the Asian Championship. 

										Furthermore, it was announced during a crowded press conference held in “La 

										Creperie” restaurant that Sanita would be the official sponsor of the National Team. 

										Among the first attendants was the Youth and Sports Minister Brigadier General 

										Abdul Muttalib Hinnawi; president of the youth and sports parliamentary committee 

										Deputy Simon Abi Ramia; FLB president Eng. Walid Nassar and members; CEO of 

										Indevco Nehme Frem and the seniors in Sanita company – Indevco group; WABA 

										secretary-general Jean Tabet; clubs presidents, international Olympic lecturer Jihad 

										Salameh; technical, medical, and administrative staff; Lebanese National Basketball 

										Team players; and a large crowd of business men and journalists.
									</p>


									<a href="https://drive.google.com/file/d/0BypjlWKMZEkuQU82bENleV95VnM/view?usp=sharing" target="_blank" class="btn btn-success" >more</a>
								</div>


								<div id="" class="news-element text-justify">

									<h3>
										Super Cup Champion: Byblos Club
									</h3>

									<p class="news-body text-justify">
										Byblos Club achieved a “historic treble” in basketball, as it scored two official titles 

										and one friendly title in less than a month. After winning Henry Chalhoub’s 

										Tournament against Al Tadamon (Zouk Mikael) in the finals and the Lebanon Cup 

										against Saggesse, Byblos – winner of the Lebanon Cup added an equal title by 

										winning the Super Cup in Late Antoine Chartier Tournament against Riyadi – 

										Lebanese league’s champion.
									</p>


									<a href="https://drive.google.com/file/d/0BypjlWKMZEkuNFIzVUZ0Tk83aHc/view?usp=sharing" target="_blank" class="btn btn-success" >more</a>
								</div>





							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

</div>
<div id="teams-container">

	<div class="row">
		<div id="divisions-data">
			<div class="divisions-top-bar">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<p id="divisions-title1">
					<strong>[National]</strong>
				</p>
				<p id="divisions-title2">
					<strong>TEAMS</strong>
				</p>
			</div>
			</div>	
		</div>
	</div>

	<div class="collection">

		<div class="row">
			<div id="data-table-teams">
				<div class="row">
					<div v-show="d1" @click="selectD1()" class="col-xs-1 col-sm-1 col-md-1 data-tabs" id="tab-0" :class="{f_tab: !tab_0_selected}">
						<h3 class="text-center">Asia Championship</h3>
					</div>
					<div v-show="d2" @click="selectD2()" class="col-xs-1 col-sm-1 col-md-1 data-tabs tabs" id="tab-1" :class="{tab_selected: tab_1_selected}">
						<h3 class="tab-title text-center">WABA Championship</h3>
					</div>
					<div v-show="d3" @click="selectD3()" class="col-xs-1 col-sm-1 col-md-1 data-tabs tabs" id="tab-2" :class="{tab_selected: tab_2_selected}">
						<h3 class="tab-title text-center">Arab Championship</h3>
					</div>
					<div v-show="d4" @click="selectD4()" class="col-xs-1 col-sm-1 col-md-1 data-tabs tabs" id="tab-3">
						<h3 class="tab-title text-center">DIVISION 04</h3>
					</div>
					<div v-show="d5" @click="selectD5()" class="col-xs-1 col-sm-1 col-md-1 data-tabs tabs" id="tab-3">
						<h3 class="tab-title text-center">DIVISION 05</h3>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 pull-right">
						<div class="row">
							<div>
								<div class="white col-xs-5 col-sm-5 col-md-5" id="clubs">
									<h4 class="text-center"></h4>
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7 white">
									<div id="links-container-t">
										<span v-bind:class="{'red':male.selected}" @click="selectMale()">MALE</span> / 
										<span v-bind:class="{'red':youth.selected}" @click="selectYouth()">YOUTH</span>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<img id="img-bar" src="/images/img-bar.png">
				</div>
				<div v-show="group=='youth' && tab_2_selected==false" class="row white">
					<h3>Under 16</h3>
				</div>
				<div v-show="group=='youth' && tab_2_selected==true" class="row white">
					<h3>Under 17</h3>
				</div>

				<div class="tab-data-container">
					<div v-for="(index, player) in table" class="row table-content">
						<div class="col-xs-12 col-sm-12 col-md-12" :class="{'second':$index%2==0, 'first':$index%2==1}">
							<div class="table-data-content">
								<div class="row">
									<div class="col-xs-1 col-sm-1 col-md-1">
										<p class="table-data-number">@{{index+1}}</p>
									</div>
									<div class="col-xs-11 col-sm-11 col-md-11">
										<p class="text-center table-data-division">@{{player.name}}</p>
									<div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

		
				
			</div>
		</div>

		<div class="team-players-row team-controls">	
			<div class="controls-container pull-right">
				<img class="control-left" src="/images/left.png">
				<img class="control-right" src="/images/right.png">
			</div>
		</div>
	</div>
</div>
<div id="committees-container">
	<div class="row data-inner">
		<div class="top-bar">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<p id="committees-title1">
					<strong>[board of]</strong>
				</p>
				<p id="committees-title2">
					<strong>COMMITTEES</strong>
				</p>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="controls-container pull-right">
					<img class="control-left"  @click="back()" src="/images/left.png">
					<img class="control-right" @click="next()" src="/images/right.png">
				</div>
			</div>
		</div>
	</div>

	<div class="images">
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="@{{img_0.image}}">
					<div class="user-data">
						<p class="user-name user-name-c">@{{img_0.name}}</p>
						<p class="user-title">[@{{img_0.position}}]</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="@{{img_1.image}}">
					<div class="user-data">
						<p class="user-name user-name-c">@{{img_1.name}}</p>
						<p class="user-title">[@{{img_1.position}}]</p>
					</div>
				</div>
			</div>
			<div v-show="img_2" class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="@{{img_2.image}}">
					<div class="user-data">
						<p class="user-name user-name-c">@{{img_2.name}}</p>
						<p class="user-title">[@{{img_2.position}}]</p>
					</div>
				</div>
			</div>
			<div v-show="img_3" class="col-xs-3 col-sm-3 col-md-3">
				<div class="item">
					<img class="user-image" src="@{{img_3.image}}">
					<div class="user-data">
						<p class="user-name user-name-c">@{{img_3.name}}</p>
						<p class="user-title">[@{{img_3.position}}]</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
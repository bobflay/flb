<div class="row">
	<div id="home-container">
		<div class="row">
			<div id="navigation-container">
				<div id="menu-container">
				</div>
			</div>
		</div>

		<div class="row">
			<div id="president-msg-container">
				<div class="row">
					<div class="president-msg">
						<div id="president-text">
									<div id="president-scroll">
										<p id="president-txt" class="text-justify">
											It is strange how this game occupied Lebanese’s heart, how much applause it got and tears has been shed.
											How it went through stages of joy and winning, however accepted the opposite.

											Basketball knew golden ages but its lines remained gold and shiny through a constellation of 

											administrators, a group of players, the impulse of investors and the enthusiasm of the crowd.

											Despite the attitudes and positions, basketball is beyond the time while continuing its victory.

											This game is meant to embrace the Lebanese youth and crowd, and motivate the Lebanese street 

											in a way that others couldn’t. 

											This game knew winning and glory moments, and we will always be faithful to its heritage, 

											victory while maintaining it.
										</p>
										
										</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<div id="contacts-container">
		<div class="top-bar row calendar-title">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<p id="directors-title1">
					<strong>[contact]</strong>
				</p>
				<p id="directors-title2">
					<strong>INFO</strong>
				</p>
			</div>
		</div>
		<div class="row white" id="contacts-data-row">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="row">
					<p id="p1" class="contacts-width grey2">
						Our team is fully dedicated to reply to any of your questions or inquiries. Don’t hesitate to contact us, or use the form below to send your message.
					</p>
				</div>
				<div id="p2" class="row">

					<div class=" row">
						<div class="col-xs-2 col-sm-2 col-md-2">
							Phone
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 red">
							+ 961 4 722 023
						</div>
					</div>
					<div class="row">
						<div class="col-xs-2 col-sm-2 col-md-2">
							Fax
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 red">
							+ 961 4 722 025 
						</div>
					</div>
					<div class="row">
						<div class="col-xs-2 col-sm-2 col-md-2">
							E-mail
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 red">
							info@flbb.com
						</div>
					</div>

				</div>
				<div class="row">
					<p id="p3" class="contacts-width grey2">
						White House Hotel, Bloc B,<br>
						3rd and 4th floor, Jal el Dib Highway
					</p>
				</div>

			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div id="mail-form-container">
					<div class="row">
						<img src="/images/contact-form.png">
					</div>
					<div class="row">
						<input type="text" class="form-padding" name="name" placeholder="name" id="name-txt">
					</div>
					<div class="row">
						<input type="text" class="form-padding" name="email" placeholder="email" id="email-txt">
					</div>
					<div class="row">
						<textarea type="text" class="form-padding" name="message" placeholder="message" id="message-txt"></textarea>
					</div>

					<div class="row" id="submit-row">
						<div class="pull-right">
							<img src="/images/send.png">
						</div>
					</div>

				</div>
			</div>
		</div>


</div>